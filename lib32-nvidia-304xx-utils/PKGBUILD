# Contributions from AUR file: https://aur.archlinux.org/packages/lib32-nvidia-utils-304xx/

_pkgbasename=nvidia-304xx-utils
pkgname=('lib32-nvidia-304xx-utils' 'lib32-opencl-nvidia-304xx')
pkgver=304.135
pkgrel=2
pkgdesc="NVIDIA drivers utilities and libraries. (32-bit)"
arch=('x86_64')
url="http://www.nvidia.com/"
depends=('lib32-libxvmc' 'lib32-zlib' 'lib32-gcc-libs' "nvidia-304xx-utils=${pkgver}")
conflicts=('lib32-libgl')
provides=('lib32-libgl')
license=('custom')
options=('!strip')

_parch='x86'
_pkg="NVIDIA-Linux-${_parch}-${pkgver}"
source=("ftp://download.nvidia.com/XFree86/Linux-${_parch}/${pkgver}/${_pkg}.run")
md5sums=('0e2082ae8490b135eb306befe6db56e1')

create_links() {
  # create soname links
  for _lib in $(find "${pkgdir}" -name '*.so*' | grep -v 'xorg/'); do
    _soname=$(dirname "${_lib}")/$(readelf -d "${_lib}" | grep -Po 'SONAME.*: \[\K[^]]*' || true)
    _base=$(echo ${_soname} | sed -r 's/(.*).so.*/\1.so/')
    [[ -e "${_soname}" ]] || ln -s $(basename "${_lib}") "${_soname}"
    [[ -e "${_base}" ]] || ln -s $(basename "${_soname}") "${_base}"
  done
}

build() {
    cd "${srcdir}"
    sh ${_pkg}.run --extract-only
}

package_lib32-opencl-nvidia-304xx() {
  pkgdesc="OpenCL implemention for NVIDIA (32-bit)"
  depends=('lib32-zlib' 'lib32-gcc-libs' 'opencl-nvidia-304xx')
  optdepends=('opencl-headers: headers necessary for OpenCL development')
  conflicts=('lib32-opencl-nvidia')
  provides=('lib32-opencl-driver')

  cd ${_pkg}

  # OpenCL
  install -D -m755 "libnvidia-compiler.so.${pkgver}" "${pkgdir}/usr/lib32/libnvidia-compiler.so.${pkgver}"
  install -D -m755 "libnvidia-opencl.so.${pkgver}" "${pkgdir}/usr/lib32/libnvidia-opencl.so.${pkgver}"
  
  create_links

  mkdir -p "${pkgdir}/usr/share/licenses"
  ln -s $_pkgbasename "${pkgdir}/usr/share/licenses/lib32-opencl-nvidia"
}

package_lib32-nvidia-304xx-utils() {
  pkgdesc="NVIDIA drivers utilities (32-bit)"
  depends=('lib32-libxvmc' 'lib32-zlib' 'lib32-gcc-libs' 'lib32-libxext' 'lib32-mesa' 'nvidia-304xx-utils')
  optdepends=('lib32-opencl-nvidia-304xx')
  conflicts=('lib32-nvidia-304xx-libgl' 'lib32-nvidia-340xx-utils' 'lib32-nvidia-utils')
  provides=('lib32-libgl'  'lib32-libegl' 'lib32-libgles' 'lib32-nvidia-304xx-libgl')
  replaces=('lib32-nvidia-304xx-libgl')

  cd ${_pkg}

  # OpenGL libraries
  install -D -m755 "libGL.so.${pkgver}" "${pkgdir}/usr/lib32/nvidia/libGL.so.${pkgver}"

  # OpenGL core library
  install -D -m755 "libnvidia-glcore.so.${pkgver}" "${pkgdir}/usr/lib32/nvidia/libnvidia-glcore.so.${pkgver}"

  # XvMC
  install -D -m755 "libXvMCNVIDIA.so.${pkgver}" "${pkgdir}/usr/lib32/nvidia/libXvMCNVIDIA.so.${pkgver}"

  # VDPAU
  install -D -m755 "libvdpau_nvidia.so.${pkgver}" "${pkgdir}/usr/lib32/vdpau/libvdpau_nvidia.so.${pkgver}"

  # nvidia-tls library
  install -D -m755 "tls/libnvidia-tls.so.${pkgver}" "${pkgdir}/usr/lib32/nvidia/libnvidia-tls.so.${pkgver}"
  install -D -m755 "libnvidia-cfg.so.${pkgver}" "${pkgdir}/usr/lib32/nvidia/libnvidia-cfg.so.${pkgver}"
  install -D -m755 "libnvidia-ml.so.${pkgver}" "${pkgdir}/usr/lib32/nvidia/libnvidia-ml.so.${pkgver}"
  
  # CUDA
  install -D -m755 "libcuda.so.${pkgver}" "${pkgdir}/usr/lib32/nvidia/libcuda.so.${pkgver}"
  install -D -m755 "libnvcuvid.so.${pkgver}" "${pkgdir}/usr/lib32/nvidia/libnvcuvid.so.${pkgver}"

  # DEBUG
  install -D -m755 nvidia-debugdump "${pkgdir}/usr/bin/nvidia-debugdump"

  # We have to provide symlinks to mesa, as nvidia 304xx doesn't ship them
  ln -s /usr/lib32/mesa/libEGL.so.1.0.0 "${pkgdir}/usr/lib32/nvidia/libEGL.so.1.0.0"
  ln -s libEGL.so.1.0.0      "${pkgdir}/usr/lib32/nvidia/libEGL.so.1"
  ln -s libEGL.so.1.0.0      "${pkgdir}/usr/lib32/nvidia/libEGL.so"

  ln -s /usr/lib32/mesa/libGLESv1_CM.so.1.1.0 "${pkgdir}/usr/lib32/nvidia/libGLESv1_CM.so.1.1.0"
  ln -s libGLESv1_CM.so.1.1.0      "${pkgdir}/usr/lib32/nvidia/libGLESv1_CM.so.1"
  ln -s libGLESv1_CM.so.1.1.0      "${pkgdir}/usr/lib32/nvidia/libGLESv1_CM.so"

  ln -s /usr/lib32/mesa/libGLESv2.so.2.0.0 "${pkgdir}/usr/lib32/nvidia/libGLESv2.so.2.0.0"
  ln -s libGLESv2.so.2.0.0      "${pkgdir}/usr/lib32/nvidia/libGLESv2.so.2"
  ln -s libGLESv2.so.2.0.0      "${pkgdir}/usr/lib32/nvidia/libGLESv2.so"

  install -dm 755 "${pkgdir}"/etc/ld.so.conf.d
  echo -e '/usr/lib32/nvidia/' > "${pkgdir}"/etc/ld.so.conf.d/00-lib32-nvidia.conf

  create_links

  rm -rf "${pkgdir}"/usr/{include,share,bin}
  mkdir -p "${pkgdir}/usr/share/licenses"
  ln -s $_pkgbasename "${pkgdir}/usr/share/licenses/${pkgname}"
}
